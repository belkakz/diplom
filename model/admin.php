<?php 
class Admin
{
	public function unans() {

		$pdo = Db::get()->pdo();
		$sth = $pdo->prepare('SELECT q.id, u.user_login AS author, q.quest, q.public, c.name AS category FROM questions q JOIN users u ON u.id=q.author JOIN category c ON c.id=q.category WHERE q.public = 0');
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}
}

 ?>