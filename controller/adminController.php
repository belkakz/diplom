<?php 
include 'model/login.php';
include 'model/admin.php';

class AdminController 
{
	public function login()
	{
		$login = new Login();
		if (count($_POST) > 0) {
			$data = $_POST;
			$idAdd = $login->auth($data);
                if ($idAdd) {
                    header('Location: ./');
                } else echo "Неправильный логин или пароль";
		}
		Db::get()->render('admin/login.php');
	}

	public function adminMenu() 
	{
		Db::get()->render('admin/adminMenu.php');
	}

	public function unans()
	{
		$functions = new Admin();
		$all = $functions->unans();
		Db::get()->render('admin/listQ.php', ['all' => $all]);

		// include ('template/admin/listQ.php');
	}
}

 ?>