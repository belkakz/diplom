<style>
	body {
		padding: 0;
		max-width: 700px;
		margin: auto;
		margin-top: 100px;
	}
</style>
<?php 
session_start();
class Db 
{   
    static $db = null;

	public static function get()
	{
        if (! self::$db) {
            self::$db = new Db();
        }
        return self::$db;
    }

	public function config()
    {
        $config = include 'config.php';
        return $config;
    }

	public function pdo(){       
		$config = $this->config();
		$pdo = new PDO(
                'mysql:host='.$config['host'].';dbname='.$config['dbname'].';charset=utf8',
                $config['user'],
                $config['pass']
            );
		return $pdo;
	}

	public function render($template, $params = [])
    {
        $fileTemplate = 'template/'.$template;
        if (is_file($fileTemplate)) {
            ob_start();
            if (count($params) > 0) {
                extract($params);
            }
            include $fileTemplate;
            echo ob_get_clean();
        }
    }
}
$pdo = Db::get()->pdo();
include './router/router.php';
 ?>