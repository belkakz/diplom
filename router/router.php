<?php 
if (isset($_GET['exit'])){
	session_destroy();
	header('Location: index.php');
}


if (!isset($_GET['c']) || !isset($_GET['a'])) {
	$controller = 'category';
    $action = 'list';
} else {
    $controller = $_GET['c'];
    $action = $_GET['a'];
}

if ((isset($_SESSION['role'])) && ($_SESSION['role'] === 'admin')) {
	include 'controller/adminController.php';
	$adminController = new AdminController();
	$adminController->adminMenu();

	if (($controller == 'adminFunc') && ($action == 'unans')) {
		$adminController->unans();
	}
}


if ($controller == 'category') {
	include 'controller/CategoryController.php';
	$categoryController = new CategoryController();
	if (isset($_GET['add'])) {
		$categoryController->addTrue();
	}
    if ($action == 'list') {
        $categoryController->getList();
    }   elseif ($action == 'add') {
        $categoryController->add();
    }
}

if ($controller == 'admin'){
	include 'controller/adminController.php';
	$adminController = new AdminController();
	if ($action == 'login') {
		$adminController->login();
	}
}


 ?>